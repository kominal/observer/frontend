import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { BehaviorSubject } from 'rxjs';
import { Event } from 'src/app/core/models/event';
import { Log } from 'src/app/core/models/log';
import { ObserverHttpService } from '../../http/observer/observer-http.service';
import { MasterData } from '../../models/master-data';

@Injectable({
	providedIn: 'root',
})
export class ObserverService {
	masterDataSubject = new BehaviorSubject<MasterData>({});
	currentMasterDataTenant: string = undefined;
	currentMasterDataSubject = new BehaviorSubject<{ projectName?: string; environmentName?: string; serviceName?: string }>({});

	constructor(private observerHttpService: ObserverHttpService, private router: Router, private userService: UserService) {
		this.userService.currentTenantIdSubject.subscribe((tenantId) => {
			if (!tenantId || this.currentMasterDataTenant === tenantId) {
				return;
			}

			this.currentMasterDataTenant = tenantId;

			this.getMasterData(tenantId).then((masterData) => {
				this.masterDataSubject.next(masterData);
				this.currentMasterDataSubject.subscribe(() => this.checkMasterData());
				this.checkMasterData();
			});
		});
	}

	private checkMasterData() {
		const masterData = this.masterDataSubject.value;
		const { projectName, environmentName, serviceName } = this.currentMasterDataSubject.value;

		if (projectName && !masterData[projectName]) {
			this.currentMasterDataSubject.next({});
			this.router.navigateByUrl('/');
		}

		if (environmentName && !masterData[projectName][environmentName]) {
			this.currentMasterDataSubject.next({});
			this.router.navigateByUrl('/');
		}

		if (serviceName && !masterData[projectName][environmentName].includes(serviceName)) {
			this.currentMasterDataSubject.next({});
			this.router.navigateByUrl('/');
		}
	}

	async getEvents(
		pagination: PaginationRequest,
		options: {
			projectName: string;
			environmentName: string;
			serviceName: string;
			start?: Date;
			end?: Date;
		}
	): Promise<PaginationResponse<Event>> {
		return this.observerHttpService.getEvents(pagination, {
			...options,
			start: options.start?.toISOString(),
			end: options.end?.toISOString(),
		});
	}

	async aggregateEvents(options: {
		projectName: string;
		environmentName: string;
		serviceName: string;
		start?: Date;
		end?: Date;
		type?: string;
		aggregation: string;
		field?: string;
	}): Promise<{ name: string; series: { name: string; value: number }[] }[]> {
		return this.observerHttpService.aggregateEvents(options);
	}

	async getLogs(
		pagination: PaginationRequest,
		options: {
			projectName: string;
			environmentName: string;
			serviceName: string;
			maxId?: string;
			start?: Date;
			end?: Date;
		}
	): Promise<PaginationResponse<Log>> {
		return this.observerHttpService.getLogs(pagination, {
			...options,
			start: options.start?.toISOString(),
			end: options.end?.toISOString(),
		});
	}

	async getMasterData(tenantId: string): Promise<MasterData> {
		return this.observerHttpService.getMasterData(tenantId);
	}
}
