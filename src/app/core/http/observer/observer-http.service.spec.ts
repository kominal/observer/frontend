import { TestBed } from '@angular/core/testing';

import { ObserverHttpService } from './observer-http.service';

describe('ObserverHttpService', () => {
  let service: ObserverHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObserverHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
