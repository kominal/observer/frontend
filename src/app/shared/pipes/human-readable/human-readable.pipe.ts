import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'humanReadable',
})
export class HumanReadablePipe implements PipeTransform {
	transform(value: string, ...args: unknown[]): unknown {
		return value
			.split(/_| |-/)
			.map((w) => w.charAt(0).toUpperCase() + w.slice(1))
			.join(' ');
	}
}
