import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationInterceptor, CoreUserServiceModule } from '@kominal/core-user-service-angular-client';
import { PapyrusClientModule } from '@kominal/papyrus-angular-client';
import { BehaviorSubject } from 'rxjs';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/components/header/header.component';
import { SharedModule } from './shared/shared.module';

export const permissionsSubject = new BehaviorSubject<string[]>([]);

@NgModule({
	declarations: [AppComponent, HeaderComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		BrowserAnimationsModule,
		SharedModule,
		CoreUserServiceModule.forRoot({
			loginRedirectUrl: '/dashboard',
			permissions: permissionsSubject,
			rolesEnabled: false,
		}),
		PapyrusClientModule.forRoot({ tenantId: '601ae42c656c657164122b24', projectName: 'observer', languages: ['de', 'en'] }),
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthenticationInterceptor,
			multi: true,
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
