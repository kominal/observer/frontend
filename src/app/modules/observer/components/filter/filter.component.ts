import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ObserverService } from 'src/app/core/services/observer/observer.service';

@Component({
	selector: 'app-filter',
	templateUrl: './filter.component.html',
	styleUrls: ['./filter.component.scss'],
})
export class FilterComponent {
	Object = Object;

	form = new FormGroup({
		projectName: new FormControl(),
		environmentName: new FormControl(),
		serviceName: new FormControl(),
	});

	constructor(public observerService: ObserverService, private router: Router, private activatedRoute: ActivatedRoute) {
		this.activatedRoute.queryParams.subscribe(async (params) => {
			this.form.patchValue(params);
		});
		this.form.valueChanges.subscribe(async () => {
			this.router.navigate(['/observer/logs'], {
				queryParams: this.form.value,
			});
		});
	}
}
