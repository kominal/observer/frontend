import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AggregationInsightsComponent } from './aggregation-insights.component';

describe('AggregationInsightsComponent', () => {
  let component: AggregationInsightsComponent;
  let fixture: ComponentFixture<AggregationInsightsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AggregationInsightsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AggregationInsightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
