import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryInsightsComponent } from './history-insights.component';

describe('HistoryInsightsComponent', () => {
  let component: HistoryInsightsComponent;
  let fixture: ComponentFixture<HistoryInsightsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryInsightsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryInsightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
