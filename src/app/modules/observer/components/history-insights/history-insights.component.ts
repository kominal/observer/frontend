import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Event } from 'src/app/core/models/event';
import { Log } from 'src/app/core/models/log';
import { ObserverService } from 'src/app/core/services/observer/observer.service';

@Component({
	selector: 'app-history-insights',
	templateUrl: './history-insights.component.html',
	styleUrls: ['./history-insights.component.scss'],
})
export class HistoryInsightsComponent implements OnInit {
	@ViewChild('lazyIndicator', { static: true }) lazyIndicator!: ElementRef<HTMLElement>;
	loading = false;

	history = new BehaviorSubject<(Event | Log)[]>([]);
	io = new IntersectionObserver(async (entries) => {
		if (entries.length > 0) {
			const { environmentName, projectName, serviceName } = this.observerService.currentMasterDataSubject.value;
			const lastIndex = this.history.value.filter((value) => value.message).length - 1;
			if (lastIndex < 0 || this.loading) {
				return;
			}

			console.log('li', lastIndex);
			this.loading = true;
			const page = await this.observerService.getLogs(
				{ itemsPerPage: 256, page: 0, sort: 'time', order: 'desc', filter: '' },
				{ projectName, environmentName, serviceName, maxId: this.history.value[lastIndex]._id }
			);

			const history = [...this.history.value, ...page.items].sort((a, b) => new Date(b.time).getTime() - new Date(a.time).getTime());
			this.history.next(history);

			this.loading = false;
		}
	});

	constructor(public observerService: ObserverService, activatedRoute: ActivatedRoute) {
		activatedRoute.params.subscribe((params) => {
			observerService.currentMasterDataSubject.next(params);
		});

		observerService.currentMasterDataSubject.subscribe(({ projectName, environmentName, serviceName }) => {
			if (projectName && environmentName && serviceName) {
				const start = new Date();
				start.setHours(-2);
				this.observerService
					.getLogs({ itemsPerPage: 256, page: 0, sort: 'time', order: 'desc', filter: '' }, { projectName, environmentName, serviceName })
					.then((page) => {
						const history = [...this.history.value, ...page.items].sort((a, b) => new Date(b.time).getTime() - new Date(a.time).getTime());
						this.history.next(history);
					});
				this.observerService
					.getEvents(
						{ itemsPerPage: 100, page: 0, sort: 'time', order: 'desc', filter: '' },
						{ projectName, environmentName, serviceName, start, end: new Date() }
					)
					.then((page) => {
						const history = [...this.history.value, ...page.items].sort((a, b) => new Date(b.time).getTime() - new Date(a.time).getTime());
						this.history.next(history);
					});
			}
		});
	}

	ngOnInit(): void {
		this.io.disconnect();
		this.io.observe(this.lazyIndicator.nativeElement);
	}
}
