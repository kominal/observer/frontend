import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EnvironmentsComponent } from './pages/environments/environments.component';
import { InsightsComponent } from './pages/insights/insights.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ServicesComponent } from './pages/services/services.component';

const routes: Routes = [
	{ path: '', component: ProjectsComponent },
	{ path: ':projectName', component: EnvironmentsComponent },
	{ path: ':projectName/:environmentName', component: ServicesComponent },
	{ path: ':projectName/:environmentName/:serviceName', component: InsightsComponent },
	{ path: '**', redirectTo: '/' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ObserverRoutingModule {}
