import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TenantService } from '@kominal/core-user-service-angular-client';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Tenant } from 'src/app/core/models/tenant';
import { ObserverService } from 'src/app/core/services/observer/observer.service';

@Component({
	selector: 'app-environments',
	templateUrl: './environments.component.html',
	styleUrls: ['./environments.component.scss'],
})
export class EnvironmentsComponent implements OnInit {
	Object = Object;
	filterControl = new FormControl();
	options: string[] = [];
	filteredOptions = new BehaviorSubject<string[]>([]);

	constructor(public observerService: ObserverService, public tenantService: TenantService<Tenant>, activatedRoute: ActivatedRoute) {
		activatedRoute.params.subscribe((params) => {
			observerService.currentMasterDataSubject.next(params);
		});
	}

	ngOnInit(): void {
		this.observerService.masterDataSubject.subscribe((value) => {
			this.options = Object.keys(value[this.observerService.currentMasterDataSubject.value.projectName] || {});
			this.filteredOptions.next(this.options);
		});

		this.filterControl.valueChanges
			.pipe(
				map((value) => {
					const filterValue = value.toLowerCase().trim();

					const filtered = this.options.filter((option) => {
						return option.toLowerCase().includes(filterValue);
					});

					return filtered;
				})
			)
			.subscribe((value) => {
				this.filteredOptions.next(value);
			});
	}
}
